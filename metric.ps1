#!/usr/bin/env pwsh

Write-Output "hw! 666`r`n $(Get-Date)"

Function CountModuleJavaSourceFiles {
    [OutputType([Int])]
    Param(
        [String] $ModuleRoot
    )

    return ( Get-ChildItem -Path $ModuleRoot -Include *.java -Recurse | Where-Object { $_.DirectoryName -match 'main' } ).Length
}


Function isModuleHomeDir{
    [OutputType([Boolean])]
    Param(
        [String] $DirectoryPath
    )

    return  Test-Path -Path "$DirectoryPath/build.gradle" -PathType Leaf
}


Function SearchForSubModules{
    [OutputType([Hashtable])]
    Param(
        [String] $ModuleRootDir
    )

    $moduleClassCountsByName = @{}

    # test for ancient pre-monomess project structure, NB: cannot just check for 'monomess' folder, because Git doesn't track folders, so you need to look inside
    if (  ( Test-Path -PathType Leaf -Path "$ModuleRootDir/settings.gradle" )  -and  ! ( Test-Path -Path "$ModuleRootDir/monomess/build.gradle" )  ) {
        $moduleClassCountsByName = @{ legacy= $( CountModuleJavaSourceFiles $ModuleRootDir ) }

    } else {

        foreach ( $directoryPath in ( Get-ChildItem -Path $ModuleRootDir -Attributes Directory -Exclude .* ) ) {
            if ( isModuleHomeDir $directoryPath ){
                $directorySourceCounts = CountModuleJavaSourceFiles $directoryPath
                $moduleClassCountsByName += @{ (Get-Item $directoryPath).PSChildName= $directorySourceCounts }
                
            }else{
                $subdirectorySourceCounts = SearchForSubModules $directoryPath
                if ( $subdirectorySourceCounts.Count ){
                    $moduleClassCountsByName += $subdirectorySourceCounts
                }
            }
        }
    }


    return $moduleClassCountsByName
}


Function ForCommits{
    [OutputType([Object[]])]
    Param(
        [String] $firstCommit,
        [String] $lastCommit,
        [scriptblock] $commitProcessorFunction
    )
    git checkout --quiet $firstCommit

    $commitFunctionResultsLog = @()
    do{ 
        $commit, $commitUnixTimestamp, $commitIsoTimestamp = ( git log --pretty="%H %at %aI" --max-count=1 ) -Split " "
        # Write-Debug "$commitIsoTimestamp $commit"
    
        $commitFunctionResultsLog += $commitProcessorFunction.invoke( $commit, $commitUnixTimestamp, $commitIsoTimestamp )

        $parentCommits = ( git log --pretty=%P --max-count=1 ) -Split ' '
    
        # the feature branch is usally first and the parent second 
        # see: https://www.designcise.com/web/tutorial/what-is-meant-by-first-parent-in-a-git-commit
        $parentCommit = $parentCommits[ -1 ].trim()
        git checkout --quiet $parentCommit
    
    } while ( $commit -ne $lastCommit )

    return $commitFunctionResultsLog
}


Function Build-ModuleSizeReport{
    param (
        [string] $start_commit,
        [string] $stop_commit
    )
    return ForCommits $start_commit $stop_commit { 
        Param(
            [String] $commit,
            [String] $commitUnixTimestamp,
            [String] $commitIsoTimestamp
        )
        @{ 
            commit = $commit
            timestamp = $commitUnixTimestamp
            module_sizes = $( SearchForSubModules . )
        } 
    }
}

Function Build-ModuleSquareScore {
    param (
        [string] $start_commit,
        [string] $stop_commit
    )
    return ForCommits $start_commit $stop_commit { 
        Param(
            [String] $commit,
            [String] $commitUnixTimestamp,
            [String] $commitIsoTimestamp
        )

        # sum of squares
        [int] $runningSumOfSquaresTotal = 0
        # sum, squared
        [int] $runningTotal = 0

        foreach ( $moduleFileCount in $( SearchForSubModules . ).Values ){
            $runningSumOfSquaresTotal += [math]::pow( $moduleFileCount , 2 )
            $runningTotal += $moduleFileCount
        }

        [int] $maxScore = [math]::pow( $runningTotal, 2)

        @{ 
            commit = $commit
            timestamp = $commitUnixTimestamp
            date = $commitIsoTimestamp
            score = $runningSumOfSquaresTotal
            maxScore = $maxScore
        } 
    }
}


Function Build-RPlotScript {
    [string] $r_script_prologue = 
@"
# based on: https://www.educba.com/line-graph-in-r/
#this is only needed for the "path_home()" function or else file will be local
library(fs)


#Create the data for chart.

# Name on PNG image.
png(file = (path_home() + "/code/modularity_metric.png"))

# Plot the line chart.
"@

    [string] $module_coupling_builder = ""
    [string] $monolith_coupling_builder = ""
    foreach( $moduleFileCount in $moduleFileCountsByCommitByTimeStamp ){
        # the data is in reverse chronological order, so it has to be prepended to be put into chronological order
        $monolith_coupling_builder = "" + ( $moduleFileCount.maxScore / 1000 ) + ", $monolith_coupling_builder"
        $module_coupling_builder   = "" + ( $moduleFileCount.score    / 1000 ) + ", $module_coupling_builder"
    }
    # trim trailing ',' to comply with R's grammar
    $monolith_coupling_builder = $monolith_coupling_builder.Substring( 0, $monolith_coupling_builder.Length - 2 )
    $module_coupling_builder   = $module_coupling_builder.Substring(   0, $module_coupling_builder.Length   - 2 )

    [string] $r_script_data = ""
    $r_script_data += "monolith_coupling <- c( $monolith_coupling_builder )`r`n"
    $r_script_data += "module_coupling <- c( $module_coupling_builder )`r`n"

    $r_script_epilogue =
@"
# Plot the bar chart.
plot(monolith_coupling, type = "o",col = "red", xlab = "Commit", ylab = "Coupling Score",
main = "Coupling Metric Over Time")
lines(module_coupling, type = "o", col = "blue")
# Add a legend
legend( 125, 2900, legend=c("Monomess", "Modules"),
col=c("red", "blue"), lty=1:2, cex=0.8,
title="Project Structure", text.font=3, bg='lightblue')

# Save the file.
dev.off()
"@

    $r_script = 
@"
$r_script_prologue
$r_script_data
$r_script_epilogue
"@

    return $r_script
}


Set-Location $HOME/code/sbb
# ./gradlew -q clean | Out-Null # less files and folders to read
# git checkout --quiet develop
git checkout --quiet 'feature/RND-9357'
git pull --quiet
$latestCommit = git log --pretty=%H --max-count=1
# This commit should have worked... but it experimentally... it doesn't.
# $commitBeforeIntroductionOfMonomess = '6b09752cde6347fe32160964aa1c9e46b802c42d'
$commitBeforeIntroductionOfMonomess = 'ec08a25e30c5ff02bd34e540c057101811e2c576'


[string] $start_commit = $latestCommit
[string] $stop_commit = $commitBeforeIntroductionOfMonomess
#[string] $start_commit = '4e2a4a1a80eed1ffdaf5876933916bf563eb81b6'
#[string] $stop_commit = '7b3961b2f141311bb97a4cb1766491ebd5338cce'


if(  !  ( Test-Path $HOME/code/module_size_by_commit.log.json )  ){
    ConvertTo-Json ( Build-ModuleSizeReport $start_commit $stop_commit ) > $HOME/code/module_size_by_commit.log.json
}


$data_file_path = "$HOME/code/module_score_by_commit.log.json"
if( Test-Path $data_file_path ){
    $moduleFileCountsByCommitByTimeStamp = Get-Content $data_file_path | ConvertFrom-Json

}else{
    $moduleFileCountsByCommitByTimeStamp = Build-ModuleSquareScore $start_commit $stop_commit
    ConvertTo-Json $moduleFileCountsByCommitByTimeStamp > $data_file_path
}

$r_script = Build-RPlotScript
Write-Output $r_script



Write-Output "$(Get-Date)`r`n done! 999"
